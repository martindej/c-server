#ifndef DEF_NOTE	 
#define DEF_NOTE

#include <iostream>
#include "musique.h"
#include "motif.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <vector>

class musique;
class motif;
class instrument;

class note
{
	public:

	note(int tonalite,int degre, int num_accord,int part,int repere, int maj_min, int num_instrument,int moti,int note_ref, int note_min, int note_max, int inc_2,int partie,int sol_basse,int instruments,musique *pt, motif *mot,instrument *in);
//	~note();

	int det_tonale();
	void vect_tonalite();
	void fct_vect_accord();
	void creer();
	void det_pos_neg(int num_note_pre);
	int det_duree_note();
	int det_ecart();
	int verif_duree_note_accords(int duree_note);
	void det_note(int num_note_pre,int accord_or_not);

	private:

	int tonalite;
	int degre;
	int num_accord;
	int part;
	int repere;
	int maj_min;
	int num_instrument;
	int num_note;
	int duree_note;
	int placement;
	int oct;
	int no;
	musique *pt;
	motif *mot;
	instrument *in;
	int tonale;
	std::vector< int > vect;
	std::vector< float > derniere_note;
	std::vector< int > vect_accord;
	int pos_neg;
	std::vector< int > rythmes;
	int nbre_rythmes;
	std::vector< int > rythmes_not;
	int nbre_rythmes_not;
	int pourc;
	int nbre_m;
	int nbre_dev;
	int d_accords;
	int moti;
	int pourc_sil;
	int note_ref;
	int note_min;
	int note_max;
	int inc_2;
	int partie;
	int sol_basse;
	int instruments;
	std::vector < int > note_;
	int somme;
};

#endif
