#include "motif.h"

using namespace std;

// constructeur ........................................................

motif::motif(int nbre_accords,int nbre_motifs,musique *pt)
	:nbre_accords(nbre_accords),nbre_motifs(nbre_motifs)
{
	this->pt=pt;

	accords_disp.assign(7, vector<int>(4, 0));
}
/*
motif::~motif() 
{
	cout << "~motif" << endl;
}*/

void motif::choix_accords_disp()
{
//	cout << "function choix_accords_disp "<< endl;
	int i,d,n,nbre_accords_diff;

	accords_disp.assign(7, vector<int>(4, 0));
	nbre_accords_diff = rand()%6 + 3 ;

	accords_disp[0][rand()%4] = 1;
	accords_disp[4][rand()%4] = 1;

	for (i=0;i<nbre_accords_diff-2;i++)
	{
		do
		{
			d = rand()%7;
			n = rand()%4;
		}
		while(accords_disp[d][n] == 1);

		accords_disp[d][n] = 1;
	}
}

int motif::get_nbre_accords()
{
//	cout << "function get_nbre_accords "<< endl;
	return nbre_accords;
}

void motif::suite_accords()
{
//	cout << "function suite_accords "<< endl;
	int i,t,d, intro, somme, nbre_m,nbre_dev,inc,inc_2 = 0,j,k;

	if(nbre_accords>30)
		d = 4;
	else
		d = 3;

	do
	{
		somme = 0;
		intro = rand()%2;

		div.assign(d + intro, vector<int>(2, 0));

		if(intro == 1)
		{
			div[0][0] = rand()%(int)(nbre_accords/(d*4)) + nbre_accords/(d*4);
			div[0][1] = 0;
			somme = div[0][0];
		}

		if(nbre_motifs == 2 && d == 3)
		{
			div[0+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d) ;
			div[0+intro][1] = 1;
			somme = somme + div[0+intro][0];

			div[1+intro][0] = rand()%(int)(nbre_accords*0.6/d) +(nbre_accords*0.6/d);
			div[1+intro][1] = 2;
			somme = somme + div[1+intro][0];

			div[2+intro][0] = div[0+intro][0];
			div[2+intro][1] = 1;
			somme = somme + div[2+intro][0];
		}

		if(nbre_motifs == 2 && d == 4)
		{
			div[0+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[0+intro][1] = 1;
			somme = somme + div[0+intro][0];

			div[1+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[1+intro][1] = 2;
			somme = somme + div[1+intro][0];

			div[2+intro][0] = div[0+intro][0];
			div[2+intro][1] = 1;
			somme = somme + div[2+intro][0];

			div[3+intro][0] = div[1+intro][0];
			div[3+intro][1] = 2;
			somme = somme + div[3+intro][0];
		}

		if(nbre_motifs == 3 && d == 3)
		{
			div[0+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[0+intro][1] = 1;
			somme = somme + div[0+intro][0];

			div[1+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[1+intro][1] = 2;
			somme = somme + div[1+intro][0];

			div[2+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[2+intro][1] = 3;
			somme = somme + div[2+intro][0];
		}

		if(nbre_motifs == 3 && d == 4)
		{
			div[0+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[0+intro][1] = 1;
			somme = somme + div[0+intro][0];

			div[1+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[1+intro][1] = 2;
			somme = somme + div[1+intro][0];

			div[2+intro][0] = div[0+intro][0];
			div[2+intro][1] = 1;
			somme = somme + div[2+intro][0];

			div[3+intro][0] = rand()%(int)(nbre_accords*0.6/d) + (nbre_accords*0.6/d);
			div[3+intro][1] = 3;
			somme = somme + div[3+intro][0];
		}

	}
	while(fabs(somme-nbre_accords)>5);

	nbre_accords = somme;
//	cout<<"nbre_accords = "<<nbre_accords<<endl;

	accords.assign(5, vector<int>(nbre_accords, 0));
	somme = 0;

	for (j=0;j<d+intro;j++)
	{
		if(j!=0)
		{
			somme = somme + div[j-1][0];
		}
		if(j == 0)
			somme = 0;

		if((j+(1-intro) == div[j][1]) || (div[j][1] == 3))
		{
			nbre_m = div[j][0]/3;
			nbre_dev = div[j][0]-nbre_m;

			choix_accords_disp();

			if(intro == 1 && j == 0)
				nbre_m = div[0][0];

			do
			{
				accords[0][somme] = rand()%7 + 1;
				accords[1][somme] = rand()%4 + 1;
				accords[2][somme] = div[j][1];
				accords[4][somme] = nbre_m;
			}
			while(verif_accords(accords[0][somme]-1,(accords[1][somme]-1)) != 1);
		
			for (i=somme + 1; i < somme + nbre_m; i++)
			{
				do
				{
					switch(accords[0][i-1])
					{
						case 1 :
							t = rand()%10 +1;
							accords[0][i] = t;

							if(t == 8 || t == 9)
								accords[0][i] = 4;

							if(t == 10 || t == 11)
								accords[0][i] = 5;
							break;

						case 2 : 
							t = rand()%5 + 1;
				
							if(t == 1 || t == 2)
								accords[0][i] = 5;

							if(t == 3 || t == 4)
								accords[0][i] = 7;

							if(t == 5)
								accords[0][i] = 4;
							break;

						case 3:
							t = rand()%8 + 1;
					
							if(t == 1 || t == 2 || t == 3)
								accords[0][i] = 6;

							if(t == 4 || t == 5 || t == 6)
								accords[0][i] = 4;

							if(t == 7)
								accords[0][i] = 5;
					
							if(t == 8)
								accords[0][i] = 2;
							break;

						case 4:
							t = rand()%9 + 1;
					
							if(t == 1 || t == 2 || t == 3)
								accords[0][i] = 5;

							if(t == 4 || t == 5 || t == 6)
								accords[0][i] = 1;

							if(t == 7)
								accords[0][i] = 6;
					
							if(t == 8)
								accords[0][i] = 2;

							if(t == 9)
								accords[0][i] = 7;
							break;

						case 5:
							t = rand()%6 + 1;
					
							if(t == 1 || t == 2 || t == 3 || t == 4)
								accords[0][i] = 1;

							if(t == 5)
								accords[0][i] = 4;
					
							if(t == 6)
								accords[0][i] = 6;
							break;

						case 6:
							t = rand()%3 + 1;
					
							if(t == 1)
								accords[0][i] = 2;

							if(t == 2)
								accords[0][i] = 5;
					
							if(t == 3)
								accords[0][i] = 4;
							break;

						case 7:
							t = rand()%2 + 1;
				
							if(t == 1)
								accords[0][i] = 1;

							if(t == 2)
								accords[0][i] = 3;
							break;
					}
					accords[1][i] = rand()%4 + 1;
					accords[2][i] = div[j][1];
				}
				while(verif_accords(accords[0][i]-1,(accords[1][i]-1)) != 1);
			}
			accords[3][somme + nbre_m-1] = 5;

			if (intro != 1 || j !=0)
			{
				do
				{
					accords[0][somme + nbre_m] = rand()%7 + 1;
					accords[1][somme + nbre_m] = rand()%4 + 1;
					accords[2][somme + nbre_m] = div[j][1];
					accords[4][somme + nbre_m] = nbre_dev;
				}
				while(verif_accords(accords[0][somme + nbre_m]-1,(accords[1][somme + nbre_m]-1)) != 1);
	
				for (i=somme + nbre_m; i < somme + div[j][0]-2; i++)
				{
					do
					{
						switch(accords[0][i-1])
						{
							case 1 :
								t = rand()%10 +1;
								accords[0][i] = t;

								if(t == 8 || t == 9)
									accords[0][i] = 4;

								if(t == 10 || t == 11)
									accords[0][i] = 5;
								break;

							case 2 : 
								t = rand()%5 + 1;
				
								if(t == 1 || t == 2)
									accords[0][i] = 5;

								if(t == 3 || t == 4)
									accords[0][i] = 7;

								if(t == 5)
									accords[0][i] = 4;
								break;

							case 3:
								t = rand()%8 + 1;
					
								if(t == 1 || t == 2 || t == 3)
									accords[0][i] = 6;

								if(t == 4 || t == 5 || t == 6)
									accords[0][i] = 4;

								if(t == 7)
									accords[0][i] = 5;
					
								if(t == 8)
									accords[0][i] = 2;
								break;

							case 4:
								t = rand()%9 + 1;
					
								if(t == 1 || t == 2 || t == 3)
									accords[0][i] = 5;

								if(t == 4 || t == 5 || t == 6)
									accords[0][i] = 1;

								if(t == 7)
									accords[0][i] = 6;
					
								if(t == 8)
									accords[0][i] = 2;

								if(t == 9)
									accords[0][i] = 7;
								break;

							case 5:
								t = rand()%6 + 1;
					
								if(t == 1 || t == 2 || t == 3 || t == 4)
									accords[0][i] = 1;

								if(t == 5)
									accords[0][i] = 4;
					
								if(t == 6)
									accords[0][i] = 6;
								break;

							case 6:
								t = rand()%3 + 1;
					
								if(t == 1)
									accords[0][i] = 2;

								if(t == 2)
									accords[0][i] = 5;
					
								if(t == 3)
									accords[0][i] = 4;
								break;

							case 7:
								t = rand()%2 + 1;
				
								if(t == 1)
									accords[0][i] = 1;

								if(t == 2)
									accords[0][i] = 3;
								break;
						}

						accords[1][i] = rand()%4 + 1;
						accords[2][i] = div[j][1];
					}
					while(verif_accords(accords[0][i]-1,(accords[1][i]-1)) != 1);
				}
				do
				{
					accords[0][somme + div[j][0]-2] = 5;
					accords[1][somme + div[j][0]-2] = rand()%4 + 1;
					accords[2][somme + div[j][0]-2] = div[j][1];
				}
				while(verif_accords(accords[0][somme + div[j][0]-2]-1,(accords[1][somme + div[j][0]-2]-1)) != 1);

				do
				{
					accords[0][somme + div[j][0]-1] = 1;
					accords[1][somme + div[j][0]-1] = rand()%4 + 1;
					accords[2][somme + div[j][0]-1] = div[j][1];
					accords[3][somme + div[j][0]-1] = 8;
				}
				while(verif_accords(accords[0][somme + div[j][0]-1]-1,(accords[1][somme + div[j][0]-1]-1)) != 1);
			}
			inc_2 = inc_2 + div[j][0];
		}
		else
		{
			inc = 0;
			while(accords[2][inc] != div[j][1])
			{
				inc++;
			}
			for(k=0;k<div[j][0];k++)
			{
				accords[0][k + inc_2] = accords[0][k + inc];
				accords[1][k + inc_2] = accords[1][k + inc];
				accords[2][k + inc_2] = accords[2][k + inc];
				accords[3][k + inc_2] = accords[3][k + inc];
				accords[4][k + inc_2] = accords[4][k + inc];
			}
			inc_2 = inc_2 + div[j][0];
		}
	}
	
//	delete pt;
}

int motif::verif_accords(int d, int n)
{
//	cout << "function verif_accords "<< endl;
	if(accords_disp[d][n] == 1)
		return 1;
	else
		return 0;
}

vector<vector<int> > motif::get_accords()
{
//	cout << "function get_accords "<< endl;

	// 0 : degre de l'accord
	// 1 : numéro de l'accord
	// 2: 
	// 3:
	return accords;
}


