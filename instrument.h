#ifndef DEF_INSTRUMENT 
#define DEF_INSTRUMENT

#include <iostream>
#include <fstream>
#include "musique.h"
#include "note.h"
#include "motif.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <vector>

class musique;
class motif;

class instrument
{
	public:

	instrument(int num_instrument,int inst_ref,int instruments, int rythme_max,int note_ref, int note_min, int note_max, int tonalite, int rythme,int sol_basse,int nbre_accords, musique *pt, motif *mot);
//	~instrument();

	void creation_partie(int maj_min);
	int get_nbre_note();
	int get_num_instrument();
	void set_partition(int m,int oct, int no,int duree_note, int sil,int volume,int note_1,int note_2,int note_3);
	int get_moti(int ligne, int colonne);
	void set_moti(int num_note, int ryth, int joue,int note_1,int note_2,int note_3);
	std::vector < int > get_rythmes_not(int t);
	void choix_rythmes(int t);
	void write_file();

	private:

	int num_instrument;
	int inst_ref;
	int instruments;
	int rythme_max;
	int note_ref;
	int note_min;
	int note_max;
	int tonalite;
	musique *pt;
	motif *mot;
	int rythme;
	int sol_basse;
	std::vector < int > compt;
	std::vector < std::vector < int > > accords;
	std::vector < std::vector < int > > mot_acc;
	std::vector < std::vector < int > > rythmes_notes_not;
	std::vector < std::vector < std::vector < float > > > parti;
	//std::vector < int > inc_2;
	int nbre_accords;
	int inc;
	int inc_2;
};

#endif
