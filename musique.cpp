#include "musique.h"

using namespace std;

/* CLASSE MUSIQUE */
/* une seule classe par musique  */

musique::musique(int tonalite, int rythme, int duree_musique,int maj_min, int nbre_motifs,int duree_accords,int nbre_sol,int nbre_basse,int nbre_bat)
	:tonalite(tonalite), rythme(rythme), duree_musique (duree_musique), maj_min(maj_min), nbre_motifs(nbre_motifs),duree_accords(duree_accords),nbre_sol(nbre_sol),nbre_basse(nbre_basse),nbre_bat(nbre_bat)
{
	if (tonalite == 0)
	{
		choix_tonalite();
	}
	if (rythme == 0)
	{
		choix_rythme();
	}
	if (maj_min == 0)
	{
		choix_maj_min();
	}
	if (nbre_motifs == 0)
	{
		choix_nbre_motifs();
	}
	if (duree_accords == 0)
	{
		choix_duree_accords();
	}
	if (duree_musique == 0)
	{
		choix_duree();
	}
	if (nbre_sol == -1)
	{
		choix_nbre_sol();
	}
	if (nbre_basse == -1)
	{
		choix_nbre_basse();
	}
	if (nbre_bat == -1)
	{
		choix_nbre_bat();
	}

	remplissage_det_inst();
}

// fonctions membres......................................................

// choix de la tonalit� parmis les 15 qu'il existe
// entre 1 et 15 inclus
void musique::choix_tonalite()
{
    tonalite = rand()%15 + 1 ;
}

// choix du nombre de motifs dans la musique, entre 2 et 3 inclus
void musique::choix_nbre_motifs()
{
    nbre_motifs = rand()%2 + 2 ;
}

// choix du rythme 
void musique::choix_rythme()
{
    rythme = rand()%161 + 40;
}

// choix de la dur�e
void musique::choix_duree()
{
    duree_musique = rand()%171 + 100;
}

// choix de la tonalit� majeure ou mineure
void musique::choix_maj_min()
{
    maj_min = rand()%2 + 1;
}

//choix du nombre d'instruments principaux
void musique::choix_nbre_sol()
{
    nbre_sol = rand()%3 + 1;
}

// choix du nombre d'instruments secondaires
void musique::choix_nbre_basse()
{
    nbre_basse = rand()%4;
}

//choix du nombre de batteries
void musique::choix_nbre_bat()
{
    nbre_bat = rand()%2;
}

//choix de la dur�e des accords, entre 2 et 8 temps inclus,cela peut correspondre a la dur�e d'une ou 2 mesures
void musique::choix_duree_accords()
{
    do 
	{
		duree_accords = rand()%7 + 2;
	}
	while(duree_accords==7||duree_accords==5);
}

// fonction principale pour la creation de la musique
void musique::creation_musique()
{
//  cout << "function creation musique "<< endl;
	int i,j,decalage = 0,max,compt,compt_top = 0,k,p,h;

	nbre_inst = nbre_sol+nbre_basse+nbre_bat; 
	a.assign(nbre_inst,0);              // "a" est un tableau qui sert de curseur pour connaitre a chaque instant quelle est notre avanc� dans la musique pour chaque instrument
	equilibre.assign(128,0);           // tableau pour choisir au mieux les instruments et calculer au mieux la repartition entre les basses, aigus et medium
	note.assign(10,0);           // creation d'un tableau representant une note a un temps t
	choix_inst.assign(nbre_inst,0);    // tableau des instruments choisis

	nbre_accords = duree_musique/duree_accords;
	if(nbre_accords<20)
	{
		nbre_accords = 20;
		duree_musique = duree_accords*nbre_accords;
	}

	cout << "tonalite = " << tonalite << endl;
	if(maj_min == 1)
		cout << " --> tonalité majeure " << endl;
	if(maj_min == 2)
		cout << " --> tonalité mineure " << endl;
	cout << "nbre_motifs = " << nbre_motifs << endl;
	cout << "rythme = " << rythme << endl;
	cout << "duree_accords = " << duree_accords << endl;
	cout << "duree musique = " << duree_musique << endl;
	cout << "nombre de solistes = " << nbre_sol << endl;
	cout << "nombre de basses = " << nbre_basse << endl;
	cout << "nombre de batteries = " << nbre_bat << endl;
	cout << "nombre d'instruments = " << nbre_inst << endl;

	/* creartion d'un objet motif*/
	/* cet objet permet de creer la base de la musique, a savoir, la suite des accords*/
	moti = new motif(nbre_accords,nbre_motifs,this);
	moti->suite_accords();

	nbre_accords = moti->get_nbre_accords();
	accords.assign(5, vector<int>(nbre_accords, 0));
	accords = moti->get_accords();

	voies.assign(nbre_inst,vector < int >(nbre_accords, 0));
	creation_vect_voies();
	
	
	duree_musique = nbre_accords*duree_accords;
	partition.resize(nbre_inst, vector<vector<float> >(10, vector<float>(16*(duree_musique +1) , 0))); // +1 pour faciliter l'ecriture midi  

	for(i=0;i<nbre_sol;i++)
	{	
		//choix_inst[i] = rand()%128;
		
		if(i==0)
		{
			choix_inst[0] = rand()%128;
		}
		else
		{
			for(k=0;k<10;k++)
			{
				compt = 0;
				choix_inst[i] = rand()%128;
				for(p=0;p<i;p++)
				{
					for(h=0;h<128;h++)
					{
						if(h<det_inst[choix_inst[i]][2] && h>det_inst[choix_inst[i]][3])
						{
							equilibre[h] = equilibre[h] + 1;
						}
					}
				}
				for(h=0;h<128;h++)
				{
					if(equilibre[h] == 1)
					{
						compt++;	
					}	
				}
				if(compt >= compt_top)
				{
					max = choix_inst[i];
					compt_top = compt;
				}
			}
			choix_inst[i] = max;
		}
		

		cout<<"intrument n°"<<i<<" = "<<choix_inst[i]<<endl;
		
		/* CREATION DE L'INSTRUMENT */
		// i = num�ro de l'instrument
		// i-decalage = instrument de reference, donne la valeur de l'instrument 
		
		inst = new instrument(i,i-decalage,choix_inst[i],det_inst[choix_inst[i]][0],det_inst[choix_inst[i]][1],det_inst[choix_inst[i]][3],det_inst[choix_inst[i]][2],tonalite,rythme,0,nbre_accords,this,moti);
		inst->creation_partie(maj_min);
		
		delete inst;

		if(choix_inst[i] < 29)
		{
			nbre_sol++;
			i++;
			decalage++;
			choix_inst.resize(nbre_basse+nbre_sol+nbre_bat);
			a.resize(nbre_basse+nbre_sol+nbre_bat);
			partition.resize(nbre_basse+nbre_sol+nbre_bat, vector<vector<float> >(10, vector<float>(16*duree_musique +1 , 0))); // +1 pour faciliter l'ecriture midi  
			inst = new instrument(i,i-decalage,128 + choix_inst[i],det_inst[128 + choix_inst[i]][0],det_inst[128 + choix_inst[i]][1],det_inst[128 + choix_inst[i]][3],det_inst[128 + choix_inst[i]][2],tonalite,rythme,0,nbre_accords,this,moti);
			inst->creation_partie(maj_min);
			
			delete inst;
		}
		
	}
	for(j=nbre_sol;j<nbre_basse+nbre_sol;j++)
	{	
	//	choix_inst[j] = rand()%128;
		
		for(k=0;k<10;k++)
		{
			compt = 0;
			choix_inst[j] = rand()%128;
			for(p=0;p<j;p++)
			{
				for(h=0;h<128;h++)
				{
					if(h<det_inst[choix_inst[j]][2] && h>det_inst[choix_inst[j]][3])
					{
						equilibre[h] = equilibre[h] + 1;
					}
				}
			}
			for(h=0;h<128;h++)
			{
				if(equilibre[h] == 1)
				{
					compt++;	
				}	
			}
			if(compt >= compt_top)
			{
				max = choix_inst[i];
				compt_top = compt;
			}
		}
		choix_inst[j] = max;

		cout<<"intrument n°"<<j<<" = "<<choix_inst[j]<<endl;
		inst = new instrument(j,j-decalage,choix_inst[j],det_inst[choix_inst[j]][0],det_inst[choix_inst[j]][1],det_inst[choix_inst[j]][3],det_inst[choix_inst[j]][2],tonalite,rythme,1,nbre_accords,this,moti);
		inst->creation_partie(maj_min);
		
		delete inst;

		if(choix_inst[j] < 29)
		{
			nbre_basse++;
			j++;
			decalage++;
			choix_inst.resize(nbre_basse+nbre_sol+nbre_bat);
			a.resize(nbre_basse+nbre_sol+nbre_bat);
			partition.resize(nbre_basse+nbre_sol+nbre_bat, vector<vector<float> >(10, vector<float>(16*duree_musique +1 , 0))); // +1 pour faciliter l'ecriture midi  
			inst = new instrument(j,j-decalage,128 + choix_inst[j],det_inst[128 + choix_inst[j]][0],det_inst[128+ choix_inst[j]][1],det_inst[128+ choix_inst[j]][3],det_inst[128+ choix_inst[j]][2],tonalite,rythme,1,nbre_accords,this,moti);
			inst->creation_partie(maj_min);
			
			delete inst;
		}
	}
	for(j=nbre_basse+nbre_sol;j<nbre_basse+nbre_sol+nbre_bat;j++)
	{
		choix_inst[j] = 200;
		//choix_inst[j] = 0;
		cout<<"intrument n°"<<j<<" = "<<choix_inst[j]<<endl;
		inst = new instrument(j,j-decalage,choix_inst[j],600,48,35,81,tonalite,rythme,1,nbre_accords,this,moti);
		inst->creation_partie(maj_min);
		
		delete inst;
	}

	nbre_inst = nbre_sol+nbre_basse+nbre_bat;

	write_midi();
	
	delete moti;
	
}

void musique::write_file()
{
	int u;

	ofstream fichier("partition.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
 
        if(fichier)
        {
		for(u=0;u<nbre_accords;u++)
		{
			fichier<<accords[0][u]<<" "<<accords[1][u]<<" "<<accords[2][u]<<" "<<accords[3][u]<<" "<<accords[4][u]<<endl;
		}

		/*for(u=0;u<a[0];u++)
		{
			fichier<<partition[0][0][u]<<" "<<partition[0][1][u]<<" "<<partition[0][2][u]<<" "<<partition[0][3][u]<<" "<<partition[0][4][u]<<" "<<partition[0][5][u]<<"           "<<partition[1][0][u]<<" "<<partition[1][1][u]<<" "<<partition[1][2][u]<<" "<<partition[1][3][u]<<" "<<partition[1][4][u]<<" "<<partition[1][5][u]<<"           "<< endl;
		}*/
 
                fichier.close();
        }
        else
                cerr << "Impossible d'ouvrir le fichier !" << endl;
}

	
void musique::write_midi()
{
//	cout << "function write midi "<< endl;
	int oct,no,u,i,t_note=1,q,compt=0,z=1,compt_1=0,k,inc,tonale, dec = 0,sil,temporaire,x;

	std::vector< int > vect(72);
	std::vector< int > vect_accord(12);

	FILE* file_midi = fopen("essai1.mid", "wb");

	if (file_midi)
	{

		unsigned long t_note_midi;
		unsigned char velocity;
		unsigned char num_note_midi;
		unsigned long length = 6;
		unsigned long length_bis = 25;
		unsigned long length_part[16];// = piano.get_nbre_note()*14 + 4;
		unsigned long length_part_3;
		unsigned short formatType = 1;
		unsigned short nbreVoies = nbre_inst+1; //nbre_inst+2;
		unsigned short PPQN = 160;//384; 
		unsigned long tempo = (1000000.0 * 60.0)/((float)rythme); //microseconds/noire 

		unsigned char ff = 0xff;
		unsigned char a_51 = 0x51;
		unsigned char num_inst[16];
		unsigned char a_03 = 0x03;
		unsigned char a_20 = 0x20;
		unsigned char a_01 = 0x01;
		unsigned char a_58 = 0x58;
		unsigned char a_04 = 0x04;
		unsigned char a_59 = 0x59;
		unsigned char a_02 = 0x02;
		unsigned char n_on[16];
		unsigned char n_off[16];
		unsigned char a_2f = 0x2f;
		unsigned char c_[16];
		unsigned char zero = 0x00;
		unsigned char num;
		unsigned char den = 2; // 2 -> 4, 3 ->8 ...etc
		unsigned char cc = 24;
		unsigned char bb = 8;
		unsigned char sf;
		unsigned char mi;
		unsigned char channel;
		unsigned char temp;

		n_on[0] = 0x91;
		n_on[1] = 0x92;
		n_on[2] = 0x93;
		n_on[3] = 0x94;
		n_on[4] = 0x95;
		n_on[5] = 0x96;
		n_on[6] = 0x97;
		n_on[7] = 0x98;
		n_on[9] = 0x99;

		n_off[0] = 0x81;
		n_off[1] = 0x82;
		n_off[2] = 0x83;
		n_off[3] = 0x84;
		n_off[4] = 0x85;
		n_off[5] = 0x86;
		n_off[6] = 0x87;
		n_off[7] = 0x88;
		n_off[9] = 0x89;
		
		c_[0] = 0xc1;
		c_[1] = 0xc2;
		c_[2] = 0xc3;
		c_[3] = 0xc4;
		c_[4] = 0xc5;
		c_[5] = 0xc6;
		c_[6] = 0xc7;
		c_[7] = 0xc8;
		c_[9] = 0xc9;
	
		unsigned long buffer;

		length = convert_endian_32(length);
		length_bis = convert_endian_32(length_bis);
		formatType = convert_endian_16(formatType);
		nbreVoies = convert_endian_16(nbreVoies);
		PPQN = convert_endian_16(PPQN);
		tempo = convert_endian_24(tempo);

		// write midi MThd chunk
		fwrite("MThd", 4, 1, file_midi);
		fwrite(&length, 4, 1, file_midi);
		fwrite(&formatType, 2, 1, file_midi);
		fwrite(&nbreVoies, 2, 1, file_midi);
		fwrite(&PPQN, 2, 1, file_midi);

		// write midi MTrk chunks et options
		fwrite("MTrk", 4, 1, file_midi);
		fwrite(&length_bis, 4, 1, file_midi);
		//tempo
		fwrite(&zero, 1, 1, file_midi);
		fwrite(&ff, 1, 1, file_midi);
		fwrite(&a_51, 1, 1, file_midi);
		fwrite(&a_03, 1, 1, file_midi);
		fwrite(&tempo, 3, 1, file_midi);

		//time signature 4/4 2/4 ...etc
		if((duree_accords == 3) || (duree_accords ==6))
			num = 3;
		else
			num = 4;

		fwrite(&zero, 1, 1, file_midi);
		fwrite(&ff, 1, 1, file_midi);
		fwrite(&a_58, 1, 1, file_midi);
		fwrite(&a_04, 1, 1, file_midi);
		fwrite(&num, 1, 1, file_midi);
		fwrite(&den, 1, 1, file_midi);
		fwrite(&cc, 1, 1, file_midi);
		fwrite(&bb, 1, 1, file_midi);

		//key signature
		sf = tonalite - 8;
		mi = maj_min - 1;
		fwrite(&zero, 1, 1, file_midi);
		fwrite(&ff, 1, 1, file_midi);
		fwrite(&a_59, 1, 1, file_midi);
		fwrite(&a_02, 1, 1, file_midi);
		fwrite(&sf, 1, 1, file_midi);
		fwrite(&mi, 1, 1, file_midi);


		fwrite(&zero, 1, 1, file_midi);
		fwrite(&ff, 1, 1, file_midi);
		fwrite(&a_2f, 1, 1, file_midi);
		fwrite(&zero, 1, 1, file_midi);

		for(i=0;i<nbre_inst;i++)
		{
			dec = 0;
			compt = 0;
			temp = (unsigned char)choix_inst[i];

			//compteur duree voie_1
			for (u=0;u<duree_musique*16;u++)
			{
				oct = partition[i][1][u];
				no = partition[i][2][u];
				sil = partition[i][0][u];

				if ((oct == partition[i][1][u+1]) && (no == partition[i][2][u+1]) && (partition[i][3][u] == 0))
				{	
					t_note++;
				}
				else
				{
					if(sil != 0)
					{
						t_note_midi = t_note*10;

						buffer = t_note_midi & 0x7F;

					   	while ( (t_note_midi >>= 7) )
					   	{
					     		buffer <<= 8;
					     		buffer |= ((t_note_midi & 0x7F) | 0x80);
					   	}

						while (z == 1)
					   	{
							compt++;

						      	if (buffer & 0x80)
							  	buffer >>= 8;
						      	else
							  	break;
						 }
	
						compt = compt + 6;

						buffer = dec & 0x7F;

					   	while ( (dec >>= 7) )
					   	{
					     		buffer <<= 8;
					     		buffer |= ((dec & 0x7F) | 0x80);
					   	}

						while (z == 1)
					   	{
							compt++;

						      	if (buffer & 0x80)
							  	buffer >>= 8;
						      	else
							  	break;
						 }

						for(x=5;x<8;x++)
						{
							if(partition[i][x][u] != 0)
							{	
								compt = compt + 8;
							}
						}

						t_note = 1;
						dec = 0;
					}
					else
					{
						dec = dec + t_note*10;
						t_note = 1;
					}
						
				}
			}
			dec = 0;

			/*if((i == nbre_inst-1) && (nbre_bat == 1))
			{
				compt = compt - 3;
			}*/

			length_part[i] = compt + 12;
			length_part[i] = convert_endian_32(length_part[i]);
			channel = i+1;

			if((i == nbre_inst-1) && (nbre_bat == 1))
			{
				channel = 10;
			} 

			fwrite("MTrk", 4, 1, file_midi);
			fwrite(&length_part[i], 4, 1, file_midi);
			//number of channel
			fwrite(&zero, 1, 1, file_midi);
			fwrite(&ff, 1, 1, file_midi);
			fwrite(&a_20, 1, 1, file_midi);
			fwrite(&a_01, 1, 1, file_midi);
			fwrite(&channel, 1, 1, file_midi);

			//if((i != nbre_inst-1) || (nbre_bat == 0))
			//{
				//instrument
				fwrite(&zero, 1, 1, file_midi);
				fwrite(&c_[i], 1, 1, file_midi);
				fwrite(&temp, 1, 1, file_midi);
			//}

			for (u=0;u<duree_musique*16;u++)
			{
				oct = partition[i][1][u];
				no = partition[i][2][u];
				sil = partition[i][0][u];
				velocity = (unsigned char)partition[i][4][u];
	
				if ((oct == partition[i][1][u+1]) && (no == partition[i][2][u+1]) && (partition[i][3][u] == 0))
				{	
					t_note++;
				}
				else
				{
					if(sil != 0)
					{
						t_note_midi = dec;

						num_note_midi = oct*12 + no + 23;

						// note on
						WriteVarLen(t_note_midi, file_midi);
						if((i == nbre_inst-1) && (nbre_bat == 1))
						{
							fwrite(&n_on[9], 1, 1, file_midi);       // note on 9(channel 10)
						}
						else
						{
							fwrite(&n_on[i], 1, 1, file_midi);
						}
						fwrite(&num_note_midi,1,1,file_midi); //numéro de la note
						fwrite(&velocity, 1, 1, file_midi); //vélocité de la note

						for(x=5;x<8;x++)
						{
							if(partition[i][x][u] != 0)
							{	
								t_note_midi = 0;

								num_note_midi = partition[i][x][u] + 23;

								// note on
								WriteVarLen(t_note_midi, file_midi);
								if((i == nbre_inst-1) && (nbre_bat == 1))
								{
									fwrite(&n_on[9], 1, 1, file_midi);       // note on 9(channel 10)
								}
								else
								{
									fwrite(&n_on[i], 1, 1, file_midi);
								}
								fwrite(&num_note_midi,1,1,file_midi); //numéro de la note
								fwrite(&velocity, 1, 1, file_midi); //vélocité de la note
							}
						}

						velocity = 0;
						t_note_midi = t_note*10; //*PPQN/16.0;
						num_note_midi = oct*12 + no + 23;

						// note off
						WriteVarLen(t_note_midi, file_midi);
						if((i == nbre_inst-1) && (nbre_bat == 1))
						{
							fwrite(&n_off[9], 1, 1, file_midi);       // note off 9(channel 10)
						}
						else
						{
							fwrite(&n_off[i], 1, 1, file_midi);
						}
						fwrite(&num_note_midi, 1, 1, file_midi); //numéro de la note
						fwrite(&velocity, 1, 1, file_midi); //vélocité de la note

						for(x=5;x<8;x++)
						{
							if(partition[i][x][u] != 0)
							{	
								t_note_midi = 0;

								num_note_midi = partition[i][x][u] + 23;

								// note on
								WriteVarLen(t_note_midi, file_midi);
								if((i == nbre_inst-1) && (nbre_bat == 1))
								{
									fwrite(&n_off[9], 1, 1, file_midi);       // note on 9(channel 10)
								}
								else
								{
									fwrite(&n_off[i], 1, 1, file_midi);
								}
								fwrite(&num_note_midi,1,1,file_midi); //numéro de la note
								fwrite(&velocity, 1, 1, file_midi); //vélocité de la note
							}
						}

						t_note = 1;
						dec = 0;
					}
					else
					{
						dec = dec + t_note*10;
						t_note = 1;
					}
				}

			}
			fwrite(&zero, 1, 1, file_midi);
			fwrite(&ff, 1, 1, file_midi);
			fwrite(&a_2f, 1, 1, file_midi);
			fwrite(&zero, 1, 1, file_midi);
		}
/*
		// calcul taille partie accords
		compt = 0,compt_1 = 0;
		t_note_midi = duree_accords*16*10;
		buffer = t_note_midi & 0x7F;

		while ( (t_note_midi >>= 7) )
		{
			buffer <<= 8;
			buffer |= ((t_note_midi & 0x7F) | 0x80);
		}

		while (z == 1)
		{
			compt_1++;

			if (buffer & 0x80)
				buffer >>= 8;
			else
				break;
		}

		for (u=0;u<nbre_accords;u++)
		{cout<<"u "<<u<<endl;
			note *note_a = new note(tonalite,accords[0][u],accords[1][u],accords[2][u],accords[3][u],maj_min,0,0,48,0,127,0,0,this,moti,inst);
cout<<"u "<<u<<endl;
			vect = note_a->vect_tonalite(vect);
cout<<"u "<<u<<endl;
			tonale = note_a->det_tonale();
cout<<"tonale "<<tonale<<endl;
			vect_accord = note_a->fct_vect_accord(vect,tonale,vect_accord);
cout<<"u "<<u<<endl;

			for(q=0;q<12;q++)
			{
				if(vect_accord[q] == 1)
				{
					compt = compt + 7 + compt_1;
				}
			}
			
		}
		length_part_3 = compt + 12;
		length_part_3 = convert_endian_32(length_part_3);
		channel = channel + 1;

		fwrite("MTrk", 4, 1, file_midi);
		fwrite(&length_part_3, 4, 1, file_midi);
		fwrite(&zero, 1, 1, file_midi);
		fwrite(&ff, 1, 1, file_midi);
		fwrite(&a_20, 1, 1, file_midi);
		fwrite(&a_01, 1, 1, file_midi);
		fwrite(&channel, 1, 1, file_midi);

		//instrument
		fwrite(&zero, 1, 1, file_midi);
		fwrite(&c_[i+1], 1, 1, file_midi);
		fwrite(&zero, 1, 1, file_midi); // piano
cout<<"test"<<endl;
		for (u=0;u<nbre_accords;u++)
		{
			note *note_a = new note(tonalite,accords[0][u],accords[1][u],accords[2][u],accords[3][u],maj_min,0,0,48,0,127,0,0,this,moti,inst);
			vect = note_a->vect_tonalite(vect);
			tonale = note_a->det_tonale();
			vect_accord = note_a->fct_vect_accord(vect,tonale,vect_accord);

			//determination note degré
			k = 0;
			inc = 0;
			do
			{
				if(vect[k] == 1)
				{
					inc++;
				}
				k++;
			}
			while(inc != accords[0][u]);
		
			oct = 3;

			for(q=0;q<12;q++)
			{
				t_note_midi = 0;
				inc = k - 1 + q;

				if (inc>11)
				{
					inc = inc - 12;
					oct = 4;
				}

				if(vect_accord[inc] == 1)
				{ 
					num_note_midi = oct*12 + inc + 23;
					velocity = 100;
	
					// note on
					WriteVarLen(t_note_midi, file_midi);
					//fwrite(&t_note_midi, 4, 1, file_midi);
					fwrite(&n_on[i+1], 1, 1, file_midi);       // note on 9(channel 1)
					fwrite(&num_note_midi,1,1,file_midi); //numéro de la note
					fwrite(&velocity, 1, 1, file_midi); //vélocité de la note
				}
			}

			t_note_midi = duree_accords*16*10;
			oct = 3;

			for(q=0;q<12;q++)
			{
		
				inc = k - 1 + q;

				if (inc>11)
				{
					inc = inc - 12;
					oct = 4;
				}

				if(vect_accord[inc] == 1)
				{ 
					num_note_midi = oct*12 + inc + 23;
					velocity = 0;
	
					// note on
					WriteVarLen(t_note_midi, file_midi);
					//fwrite(&t_note_midi, 4, 1, file_midi);
					fwrite(&n_off[i+1], 1, 1, file_midi);       // note on 9(channel 1)
					fwrite(&num_note_midi,1,1,file_midi); //numéro de la note
					fwrite(&velocity, 1, 1, file_midi); //vélocité de la note
					t_note_midi = 0;
				}

			}
			//delete note_a ;
		}

		fwrite(&zero, 1, 1, file_midi);
		fwrite(&ff, 1, 1, file_midi);
		fwrite(&a_2f, 1, 1, file_midi);
		fwrite(&zero, 1, 1, file_midi);*/

		printf("Saved midi\n");
		fclose(file_midi);
	}
	else
		printf("Could not open to write midi data\n");
	
}

int musique::valeur_vect_voies(int i,int j)
{
//	cout << "function valeur_vect_voies "<< endl;
	return voies[i][j];
}

void musique::creation_vect_voies()
{
//	cout << "function creation_vect_voies "<< endl;
	int i,j,k=0,somme = 0,decalage,dec,g,d,al,test,p,deb_motif,w,nbre_1,nbre_2,nbre_3,nbre_4;
	
	vector < vector< int > > decre(nbre_motifs, vector< int >(2*nbre_sol,0));

	for(i=0;i<nbre_motifs;i++)
	{
		for(j=0;j<nbre_sol;j++)
		{
			if(j==0)
			{	
				decre[i][j] = rand()%26 + 75;
				decre[i][j+nbre_sol] = rand()%16;
			}
			else
			{
				decre[i][j] = rand()%81 + 20;
				decre[i][j+nbre_sol] = rand()%51 +10;
			}
		}
	}

	for(i=nbre_sol;i<(nbre_sol+nbre_basse);i++)
	{
		for(j=0;j<nbre_accords;j++)
		{
			voies[i][j] = 2;
			if(j == 0)
				voies[i][j] = 3;
		}
	}

	if(nbre_basse == 0 && accords[2][0] == 0)
	{
		k = 0;
		do
		{
			nbre_3 = rand()%nbre_sol+1;
						
			for(j=0;j<nbre_3;j++)
			{
				nbre_4 = rand()%nbre_sol;
				voies[nbre_4][k] = 2;
			}
			k++;
		}
		while(accords[2][k] == 0);


		for(i=0;i<nbre_sol;i++)
		{
			for(j=0;j<k;j++)
			{
				deb_motif = 0;
				if(voies[i][j] == 2 && deb_motif==0)
				{
					voies[i][j] = 3;
					deb_motif = 1;
				}
			}
		}
	}

	for(i=0;i<nbre_motifs;i++)
	{
		k=0;
		test = 0;

		do 
		{
			if((accords[2][k] == (i+1)) && (accords[4][k]>0))
			{
				somme = accords[4][k] + accords[4][k+accords[4][k]];
			
				if(test == 0)
				{
					dec = rand()%nbre_sol;
				}

				for(j=dec;j<(nbre_sol+dec);j++)
				{
					d = 1000;
					g = j;
					if(g>=nbre_sol)
					{
						g = g-nbre_sol;
					}

					nbre_1 = (decre[i][g]/100.0)*somme;
					nbre_2 = (decre[i][g+nbre_sol]/100.0)*somme;

					decalage = rand()%(somme-nbre_1 + 1);

					for(p=decalage;p<(nbre_1+decalage);p++)
					{
						voies[j-dec][k+p] = 1;
					}

					if(nbre_2 > 0)
					{
						do
						{
							al = rand()%somme;
							if(voies[j-dec][k+al] == 0)
							{
								voies[j-dec][k+al] = 2;
								nbre_2 = nbre_2 - 1;
							}
							d--;
						}
						while(nbre_2>0 && d>0);
					}
				}

				if(nbre_basse == 0)
				{
					for(w=k;w<k+somme;w++)
					{
						test = 0;
						for(j=0;j<nbre_sol;j++)
						{		
							if(voies[j][w] != 0 )
							{
								test = 1;
							}
						}
						if(test == 0)
						{
							nbre_3 = rand()%nbre_sol;
						
							for(j=0;j<nbre_3;j++)
							{
								nbre_4 = rand()%nbre_sol;
								voies[nbre_4][w] = 2;
							}
						}
						
					}
				}

				for(j=0;j<nbre_sol;j++)
				{
					deb_motif = 0;
					for(w=k;w<k+somme;w++)
					{		
						if(voies[j][w] == 2 && deb_motif==0)
						{
							voies[j][w] = 3;
							deb_motif = 1;
						}
					}
				}
			
				test = 1;
				k = k + somme;
			}
			else
			{
				k++;
			}
		}
		while(k<nbre_accords);
	}	

	if(nbre_basse > 0)
	{
		k = 1;
		do
		{
			test = 0;
			for(j=0;j<nbre_sol;j++)
			{		
				if(voies[j][k] != 0 )
				{
					test = 1;
				}
			}
			if(test == 0)
			{
				nbre_3 = rand()%(nbre_basse+1);
			
				for(j=0;j<nbre_3;j++)
				{
					nbre_4 = rand()%nbre_sol;
					voies[nbre_4][k] = 1;
				}
			}
			k++;
		}
		while(k<nbre_accords);
	}

	for(i=(nbre_sol+nbre_basse);i<nbre_inst;i++)
	{
		for(j=0;j<nbre_accords;j++)
		{
			voies[i][j] = 4;
			if(j == 0)
				voies[i][j] = 3;
		}
	}

		/*for(j=0;j<nbre_accords;j++)
		{
			cout<<voies[0][j]<<" "<<voies[1][j]<<" "<<voies[2][j]<<" "<<voies[3][j]<<" "<<endl;
		}*/
}

void musique::WriteVarLen(unsigned long value, FILE *file_midi)
{
//	cout << "function writevarlen "<< endl;
   	unsigned long buffer;
   	buffer = value & 0x7F;
	int z = 1;

   	while ( (value >>= 7) )
   	{
     		buffer <<= 8;
     		buffer |= ((value & 0x7F) | 0x80);
   	}

	while (z == 1)
   	{
		fwrite(&buffer, 1, 1, file_midi);

	      	if (buffer & 0x80)
		  	buffer >>= 8;
	      	else
		  	break;
	 }
}

unsigned long musique::convert_endian_32(unsigned long input)
{
	unsigned long output;	

	output = ((input>>24)&0xff) | // move byte 3 to byte 0
                    ((input<<8)&0xff0000) | // move byte 1 to byte 2
                    ((input>>8)&0xff00) | // move byte 2 to byte 1
                    ((input<<24)&0xff000000); // byte 0 to byte 3

	return output;
}

unsigned long musique::convert_endian_24(unsigned long input)
{
	unsigned long output;	

	output = ((input>>16)&0xff) | 
                    ((input)&0xff00) |
                    ((input<<16)&0xff0000); 

	return output;
}
/*
unsigned long musique::convert_endian(unsigned long input)
{
	unsigned long output;	

	output = ((input>>40)&0xff) | //move byte 5 to 0
		 ((input<<8)&0xff00000000) | // move byte 4 to byte 1
                 ((input<<16)&0xff000000) | // move byte 3 to byte 2
                 ((input<<24)&0xff0000) | // move byte 2 to byte 3
                 ((input<<32)&0xff00) | // byte 1 to byte 4
		 ((input<<40)&0xff);

	return output;
}
*/

unsigned short musique::convert_endian_16(unsigned short input)
{
	unsigned short output;	

	output = (input>>8) | (input<<8);

	return output;
}

vector<float> musique::get_note(int temps, int num_instrument)
{	
	int i;

	for(i=0;i<10;i++)
	{
		note[i] = partition[num_instrument][i][temps];
	}

	return note;
}

int musique::get_duree_note(int temps, int num_instrument)
{	
//	cout << "function get_duree_note "<< endl;
	int duree = 0;

	do
	{
		temps--;
		duree ++;
	}
	while(partition[num_instrument][3][temps] != 1 && temps > -1);

	return duree;
}

void musique::add_parti(std::vector < std::vector < float > > parti, int c, int voie)
{
//	cout << "function add_parti "<< endl;
	int i,k;

	for(i=0;i<c;i++)
	{
		for(k=0;k<10;k++)
		{
			partition[voie][k][a[voie]+i] = parti[k][i];
		}
	}
	a[voie] = a[voie] + c;
}

void musique::set_partition(int oct, int no,int duree_note, int num_instrument, int sil,int volume,int note_1,int note_2,int note_3)
{
//	cout << "function set_partition "<< endl;
	int u = 0, duree_note_1;

	if(duree_note == 3)
		duree_note_1 = 4;
	if(duree_note == 4)
		duree_note_1 = 8;
	if(duree_note == 5)
		duree_note_1 = 16;
	if(duree_note == 6)
		duree_note_1 = 24;
	if(duree_note == 7)
		duree_note_1 = 32;
	if(duree_note == 8)
		duree_note_1 = 48;
	if(duree_note == 9)
		duree_note_1 = 64;

	duree_note = duree_note_1;
	
	if(a[num_instrument]+duree_note > 16*(duree_musique +1))
	{
		partition.resize(nbre_basse+nbre_sol+nbre_bat, vector<vector<float> >(10, vector<float>(a[num_instrument]+duree_note , 0)));
	}

	for (u=a[num_instrument];u<a[num_instrument]+duree_note;u++)
	{
		partition[num_instrument][0][u] = sil;          //silence
		partition[num_instrument][1][u] = oct;	        //octave
		partition[num_instrument][2][u] = no;           //num note
		partition[num_instrument][3][u] = 0;            //indicateur fin duree note
		partition[num_instrument][4][u] = volume;            //volume
		partition[num_instrument][5][u] = note_1;           // note supplémentaires accords 
		partition[num_instrument][6][u] = note_2;
		partition[num_instrument][7][u] = note_3;
		partition[num_instrument][8][u] = 1;
		partition[num_instrument][9][u] = 1;            //details
	}
	partition[num_instrument][3][a[num_instrument]+duree_note-1] = 1;

	a[num_instrument] = u;
}

int musique::get_a(int num_instrument)
{
//	cout << "function get_a "<< endl;
	return a[num_instrument];
}

int musique::get_duree_musique()
{
//	cout << "function get_duree_musique "<< endl;
	return duree_musique;
}

int musique::get_duree_accords()
{
//	cout << "function get_duree_accords "<< endl;
	return duree_accords;
}

void musique::remplissage_det_inst()
{
//	cout << "function remplissage_det_inst"<< endl;
	det_inst.resize(155, vector<int>(4));

	//piano grand acoustique voie haute
	det_inst[0][0] = 600;     // rythme max absolu
	det_inst[0][1] = 48;     //note reference
	det_inst[0][2] = 72;      // note max
	det_inst[0][3] = 30;      // note min

	//piano acoustique
	det_inst[1][0] = 600;     // rythme max absolu
	det_inst[1][1] = 48;     //note reference
	det_inst[1][2] = 72;      // note max
	det_inst[1][3] = 30;      // note min

	//piano grand electrique
	det_inst[2][0] = 600;     // rythme max absolu
	det_inst[2][1] = 47;     //note reference
	det_inst[2][2] = 64;      // note max
	det_inst[2][3] = 30;      // note min

	//piano bastringue
	det_inst[3][0] = 600;     // rythme max absolu
	det_inst[3][1] = 47;     //note reference
	det_inst[3][2] = 64;      // note max
	det_inst[3][3] = 30;      // note min

	//piano electrique
	det_inst[4][0] = 600;     // rythme max absolu
	det_inst[4][1] = 47;     //note reference
	det_inst[4][2] = 64;      // note max
	det_inst[4][3] = 30;      // note min

	//piano effet chorus
	det_inst[5][0] = 400;     // rythme max absolu
	det_inst[5][1] = 47;     //note reference
	det_inst[5][2] = 64;      // note max
	det_inst[5][3] = 30;      // note min

	//clavecin
	det_inst[6][0] = 500;     // rythme max absolu
	det_inst[6][1] = 47;     //note reference
	det_inst[6][2] = 64;      // note max
	det_inst[6][3] = 30;      // note min

	//clavinet
	det_inst[7][0] = 500;     // rythme max absolu
	det_inst[7][1] = 47;     //note reference
	det_inst[7][2] = 64;      // note max
	det_inst[7][3] = 30;      // note min

	//celesta
	det_inst[8][0] = 800;     // rythme max absolu
	det_inst[8][1] = 47;     //note reference
	det_inst[8][2] = 64;      // note max
	det_inst[8][3] = 30;      // note min

	//Glockenspiel
	det_inst[9][0] = 800;     // rythme max absolu
	det_inst[9][1] = 47;     //note reference
	det_inst[9][2] = 64;      // note max
	det_inst[9][3] = 30;      // note min

	//boite a musique
	det_inst[10][0] = 800;     // rythme max absolu
	det_inst[10][1] = 47;     //note reference
	det_inst[10][2] = 64;      // note max
	det_inst[10][3] = 30;      // note min

	//Vibraphone
	det_inst[11][0] = 800;     // rythme max absolu
	det_inst[11][1] = 47;     //note reference
	det_inst[11][2] = 64;      // note max
	det_inst[11][3] = 30;      // note min

	//marimba
	det_inst[12][0] = 800;     // rythme max absolu
	det_inst[12][1] = 47;     //note reference
	det_inst[12][2] = 64;      // note max
	det_inst[12][3] = 30;      // note min

	//Xylophone
	det_inst[13][0] = 800;     // rythme max absolu
	det_inst[13][1] = 47;     //note reference
	det_inst[13][2] = 64;      // note max
	det_inst[13][3] = 30;      // note min

	//cloches tubulaires
	det_inst[14][0] = 800;     // rythme max absolu
	det_inst[14][1] = 47;     //note reference
	det_inst[14][2] = 64;      // note max
	det_inst[14][3] = 30;      // note min

	//Tympanon
	det_inst[15][0] = 800;     // rythme max absolu
	det_inst[15][1] = 47;     //note reference
	det_inst[15][2] = 64;      // note max
	det_inst[15][3] = 30;      // note min

	//Orgue hammond
	det_inst[16][0] = 500;     // rythme max absolu
	det_inst[16][1] = 47;     //note reference
	det_inst[16][2] = 64;      // note max
	det_inst[16][3] = 30;      // note min

	//Orgue a percussion
	det_inst[17][0] = 500;     // rythme max absolu
	det_inst[17][1] = 47;     //note reference
	det_inst[17][2] = 64;      // note max
	det_inst[17][3] = 30;      // note min

	//Orgue Rock
	det_inst[18][0] = 500;     // rythme max absolu
	det_inst[18][1] = 47;     //note reference
	det_inst[18][2] = 64;      // note max
	det_inst[18][3] = 30;      // note min

	//grandes orgues
	det_inst[19][0] = 300;     // rythme max absolu
	det_inst[19][1] = 47;     //note reference
	det_inst[19][2] = 64;      // note max
	det_inst[19][3] = 30;      // note min

	//harmonium
	det_inst[20][0] = 300;     // rythme max absolu
	det_inst[20][1] = 47;     //note reference
	det_inst[20][2] = 64;      // note max
	det_inst[20][3] = 30;      // note min

	//accordeon
	det_inst[21][0] = 600;     // rythme max absolu
	det_inst[21][1] = 47;     //note reference
	det_inst[21][2] = 64;      // note max
	det_inst[21][3] = 30;      // note min

	//Harmonica
	det_inst[22][0] = 400;     // rythme max absolu
	det_inst[22][1] = 47;     //note reference
	det_inst[22][2] = 60;      // note max
	det_inst[22][3] = 36;      // note min

	//accordeon tango
	det_inst[23][0] = 600;     // rythme max absolu
	det_inst[23][1] = 47;     //note reference
	det_inst[23][2] = 64;      // note max
	det_inst[23][3] = 30;      // note min

	//guitare classique
	det_inst[24][0] = 600;     // rythme max absolu
	det_inst[24][1] = 47;     //note reference
	det_inst[24][2] = 63;      // note max
	det_inst[24][3] = 30;      // note min

	//guitare seche
	det_inst[25][0] = 600;     // rythme max absolu
	det_inst[25][1] = 42;     //note reference
	det_inst[25][2] = 63;      // note max
	det_inst[25][3] = 30;      // note min

	//guitare electrique - jazz
	det_inst[26][0] = 600;     // rythme max absolu
	det_inst[26][1] = 47;     //note reference
	det_inst[26][2] = 63;      // note max
	det_inst[26][3] = 30;      // note min

	//guitare electrique - son clair
	det_inst[27][0] = 600;     // rythme max absolu
	det_inst[27][1] = 47;     //note reference
	det_inst[27][2] = 63;      // note max
	det_inst[27][3] = 30;      // note min

	//guitare electrique - sourdine
	det_inst[28][0] = 600;     // rythme max absolu
	det_inst[28][1] = 47;     //note reference
	det_inst[28][2] = 63;      // note max
	det_inst[28][3] = 30;      // note min

	//guitare saturee
	det_inst[29][0] = 600;     // rythme max absolu
	det_inst[29][1] = 47;     //note reference
	det_inst[29][2] = 63;      // note max
	det_inst[29][3] = 30;      // note min

	//guitare distorssion
	det_inst[30][0] = 600;     // rythme max absolu
	det_inst[30][1] = 47;     //note reference
	det_inst[30][2] = 63;      // note max
	det_inst[30][3] = 30;      // note min

	//harmoniques guitare
	det_inst[31][0] = 600;     // rythme max absolu
	det_inst[31][1] = 47;     //note reference
	det_inst[31][2] = 63;      // note max
	det_inst[31][3] = 30;      // note min

	//basse acoustique sans frete 
	det_inst[32][0] = 600;     // rythme max absolu
	det_inst[32][1] = 24;     //note reference
	det_inst[32][2] = 40;      // note max
	det_inst[32][3] = 4;      // note min

	//basse electrique
	det_inst[33][0] = 600;     // rythme max absolu
	det_inst[33][1] = 24;     //note reference
	det_inst[33][2] = 40;      // note max
	det_inst[33][3] = 4;      // note min

	//basse electrique mediator 
	det_inst[34][0] = 600;     // rythme max absolu
	det_inst[34][1] = 24;     //note reference
	det_inst[34][2] = 40;      // note max
	det_inst[34][3] = 4;      // note min

	//basse sans frettes 
	det_inst[35][0] = 600;     // rythme max absolu
	det_inst[35][1] = 24;     //note reference
	det_inst[35][2] = 40;      // note max
	det_inst[35][3] = 4;      // note min

	//basse slap 1 
	det_inst[36][0] = 600;     // rythme max absolu
	det_inst[36][1] = 24;     //note reference
	det_inst[36][2] = 40;      // note max
	det_inst[36][3] = 4;      // note min

	//basse slap 2
	det_inst[37][0] = 600;     // rythme max absolu
	det_inst[37][1] = 24;     //note reference
	det_inst[37][2] = 40;      // note max
	det_inst[37][3] = 4;      // note min

	//basse synthe 1
	det_inst[38][0] = 600;     // rythme max absolu
	det_inst[38][1] = 24;     //note reference
	det_inst[38][2] = 40;      // note max
	det_inst[38][3] = 4;      // note min

	//basse synthe 2
	det_inst[39][0] = 600;     // rythme max absolu
	det_inst[39][1] = 24;     //note reference
	det_inst[39][2] = 40;      // note max
	det_inst[39][3] = 4;      // note min

	//violon
	det_inst[40][0] = 600;     // rythme max absolu
	det_inst[40][1] = 45;     //note reference
	det_inst[40][2] = 65;      // note max
	det_inst[40][3] = 31;      // note min

	//violon alto
	det_inst[41][0] = 600;     // rythme max absolu
	det_inst[41][1] = 45;     //note reference
	det_inst[41][2] = 65;      // note max
	det_inst[41][3] = 31;      // note min

	//violoncelle
	det_inst[42][0] = 600;     // rythme max absolu
	det_inst[42][1] = 24;     //note reference
	det_inst[42][2] = 40;      // note max
	det_inst[42][3] = 12;      // note min

	//contre basse
	det_inst[43][0] = 500;     // rythme max absolu
	det_inst[43][1] = 24;     //note reference
	det_inst[43][2] = 48;      // note max
	det_inst[43][3] = 10;      // note min

	//cordes tremolo
	det_inst[44][0] = 600;     // rythme max absolu
	det_inst[44][1] = 45;     //note reference
	det_inst[44][2] = 65;      // note max
	det_inst[44][3] = 31;      // note min

	//cordes pizzicato
	det_inst[45][0] = 600;     // rythme max absolu
	det_inst[45][1] = 45;     //note reference
	det_inst[45][2] = 65;      // note max
	det_inst[45][3] = 31;      // note min

	//harpe
	det_inst[46][0] = 500;     // rythme max absolu
	det_inst[46][1] = 45;     //note reference
	det_inst[46][2] = 65;      // note max
	det_inst[46][3] = 31;      // note min

	//Timbales
	det_inst[47][0] = 600;     // rythme max absolu
	det_inst[47][1] = 45;     //note reference
	det_inst[47][2] = 65;      // note max
	det_inst[47][3] = 31;      // note min

	//ensemble acoustique corde 1
	det_inst[48][0] = 500;     // rythme max absolu
	det_inst[48][1] = 45;     //note reference
	det_inst[48][2] = 65;      // note max
	det_inst[48][3] = 31;      // note min

	//ensemble acoustique corde 2
	det_inst[49][0] = 400;     // rythme max absolu
	det_inst[49][1] = 45;     //note reference
	det_inst[49][2] = 65;      // note max
	det_inst[49][3] = 31;      // note min

	//cordes synthe 1
	det_inst[50][0] = 500;     // rythme max absolu
	det_inst[50][1] = 45;     //note reference
	det_inst[50][2] = 65;      // note max
	det_inst[50][3] = 31;      // note min

	//cordes synthe 2
	det_inst[51][0] = 500;     // rythme max absolu
	det_inst[51][1] = 45;     //note reference
	det_inst[51][2] = 65;      // note max
	det_inst[51][3] = 31;      // note min

	//coeur aah
	det_inst[52][0] = 400;     // rythme max absolu
	det_inst[52][1] = 45;     //note reference
	det_inst[52][2] = 65;      // note max
	det_inst[52][3] = 31;      // note min

	//coeur ooh
	det_inst[53][0] = 400;     // rythme max absolu
	det_inst[53][1] = 45;     //note reference
	det_inst[53][2] = 65;      // note max
	det_inst[53][3] = 31;      // note min

	//voix synthe
	det_inst[54][0] = 500;     // rythme max absolu
	det_inst[54][1] = 45;     //note reference
	det_inst[54][2] = 65;      // note max
	det_inst[54][3] = 31;      // note min
	
	//coup d'orchestre
	det_inst[55][0] = 500;     // rythme max absolu
	det_inst[55][1] = 45;     //note reference
	det_inst[55][2] = 65;      // note max
	det_inst[55][3] = 31;      // note min

	//trompette
	det_inst[56][0] = 500;     // rythme max absolu
	det_inst[56][1] = 48;     //note reference
	det_inst[56][2] = 64;      // note max
	det_inst[56][3] = 30;      // note min

	//trombone
	det_inst[57][0] = 400;     // rythme max absolu
	det_inst[57][1] = 50;     //note reference
	det_inst[57][2] = 67;      // note max
	det_inst[57][3] = 36;      // note min

	//tuba
	det_inst[58][0] = 400;     // rythme max absolu
	det_inst[58][1] = 45;     //note reference
	det_inst[58][2] = 65;      // note max
	det_inst[58][3] = 31;      // note min

	//trompette sourdine
	det_inst[59][0] = 500;     // rythme max absolu
	det_inst[59][1] = 48;     //note reference
	det_inst[59][2] = 64;      // note max
	det_inst[59][3] = 30;      // note min

	//cor d'harmonie
	det_inst[60][0] = 400;     // rythme max absolu
	det_inst[60][1] = 45;     //note reference
	det_inst[60][2] = 65;      // note max
	det_inst[60][3] = 31;      // note min

	//section cuivres
	det_inst[61][0] = 400;     // rythme max absolu
	det_inst[61][1] = 45;     //note reference
	det_inst[61][2] = 65;      // note max
	det_inst[61][3] = 31;      // note min

	//cuivres synthe 1
	det_inst[62][0] = 400;     // rythme max absolu
	det_inst[62][1] = 45;     //note reference
	det_inst[62][2] = 65;      // note max
	det_inst[62][3] = 31;      // note min

	//cuivres synthe 2
	det_inst[63][0] = 400;     // rythme max absolu
	det_inst[63][1] = 45;     //note reference
	det_inst[63][2] = 65;      // note max
	det_inst[63][3] = 31;      // note min

	//sax soprano
	det_inst[64][0] = 600;     // rythme max absolu
	det_inst[64][1] = 45;     //note reference
	det_inst[64][2] = 62;      // note max
	det_inst[64][3] = 32;      // note min

	//sax alto
	det_inst[65][0] = 600;     // rythme max absolu
	det_inst[65][1] = 40;     //note reference
	det_inst[65][2] = 56;      // note max
	det_inst[65][3] = 25;      // note min

	//sax tenor
	det_inst[66][0] = 600;     // rythme max absolu
	det_inst[66][1] = 35;     //note reference
	det_inst[66][2] = 51;      // note max
	det_inst[66][3] = 20;      // note min

	//sax baryton
	det_inst[67][0] = 500;     // rythme max absolu
	det_inst[67][1] = 28;     //note reference
	det_inst[67][2] = 44;      // note max
	det_inst[67][3] = 13;      // note min

	//hautbois
	det_inst[68][0] = 500;     // rythme max absolu
	det_inst[68][1] = 48;     //note reference
	det_inst[68][2] = 69;      // note max
	det_inst[68][3] = 34;      // note min

	//cor anglais
	det_inst[69][0] = 500;     // rythme max absolu
	det_inst[69][1] = 48;     //note reference
	det_inst[69][2] = 65;      // note max
	det_inst[69][3] = 34;      // note min

	//basson
	det_inst[70][0] = 500;     // rythme max absolu
	det_inst[70][1] = 28;     //note reference
	det_inst[70][2] = 48;      // note max
	det_inst[70][3] = 10;      // note min

	//clarinette
	det_inst[71][0] = 600;     // rythme max absolu
	det_inst[71][1] = 46;     //note reference
	det_inst[71][2] = 67;      // note max
	det_inst[71][3] = 26;      // note min

	//picolo
	det_inst[72][0] = 600;     // rythme max absolu
	det_inst[72][1] = 54;     //note reference
	det_inst[72][2] = 69;      // note max
	det_inst[72][3] = 40;      // note min

	//flute
	det_inst[73][0] = 600;     // rythme max absolu
	det_inst[73][1] = 48;     //note reference
	det_inst[73][2] = 60;      // note max
	det_inst[73][3] = 36;      // note min

	//flute a bec
	det_inst[74][0] = 500;     // rythme max absolu
	det_inst[74][1] = 48;     //note reference
	det_inst[74][2] = 60;      // note max
	det_inst[74][3] = 36;      // note min

	//flute de pan
	det_inst[75][0] = 600;     // rythme max absolu
	det_inst[75][1] = 48;     //note reference
	det_inst[75][2] = 67;      // note max
	det_inst[75][3] = 36;      // note min

	//bouteille soufle
	det_inst[76][0] = 600;     // rythme max absolu
	det_inst[76][1] = 48;     //note reference
	det_inst[76][2] = 67;      // note max
	det_inst[76][3] = 36;      // note min

	//shakuhachi
	det_inst[77][0] = 500;     // rythme max absolu
	det_inst[77][1] = 48;     //note reference
	det_inst[77][2] = 67;      // note max
	det_inst[77][3] = 36;      // note min

	//sifflet
	det_inst[78][0] = 600;     // rythme max absolu
	det_inst[78][1] = 48;     //note reference
	det_inst[78][2] = 67;      // note max
	det_inst[78][3] = 36;      // note min

	//ocarina
	det_inst[79][0] = 600;     // rythme max absolu
	det_inst[79][1] = 48;     //note reference
	det_inst[79][2] = 67;      // note max
	det_inst[79][3] = 36;      // note min

	//signal carre 
	det_inst[80][0] = 600;     // rythme max absolu
	det_inst[80][1] = 47;     //note reference
	det_inst[80][2] = 72;      // note max
	det_inst[80][3] = 24;      // note min
	
	//signal dent de scie 
	det_inst[81][0] = 600;     // rythme max absolu
	det_inst[81][1] = 47;     //note reference
	det_inst[81][2] = 72;      // note max
	det_inst[81][3] = 24;      // note min

	//orgue a vapeur 
	det_inst[82][0] = 600;     // rythme max absolu
	det_inst[82][1] = 47;     //note reference
	det_inst[82][2] = 72;      // note max
	det_inst[82][3] = 24;      // note min

	//chiffer
	det_inst[83][0] = 600;     // rythme max absolu
	det_inst[83][1] = 47;     //note reference
	det_inst[83][2] = 72;      // note max
	det_inst[83][3] = 24;      // note min

	//charang 
	det_inst[84][0] = 600;     // rythme max absolu
	det_inst[84][1] = 47;     //note reference
	det_inst[84][2] = 72;      // note max
	det_inst[84][3] = 24;      // note min

	//voix solo
	det_inst[85][0] = 600;     // rythme max absolu
	det_inst[85][1] = 47;     //note reference
	det_inst[85][2] = 72;      // note max
	det_inst[85][3] = 24;      // note min

	//signal dent de scie quite 
	det_inst[86][0] = 600;     // rythme max absolu
	det_inst[86][1] = 47;     //note reference
	det_inst[86][2] = 72;      // note max
	det_inst[86][3] = 24;      // note min

	//signal basse solo
	det_inst[87][0] = 600;     // rythme max absolu
	det_inst[87][1] = 47;     //note reference
	det_inst[87][2] = 72;      // note max
	det_inst[87][3] = 24;      // note min

	//fantaisie 
	det_inst[88][0] = 600;     // rythme max absolu
	det_inst[88][1] = 47;     //note reference
	det_inst[88][2] = 72;      // note max
	det_inst[88][3] = 24;      // note min

	//son chaleureux
	det_inst[89][0] = 600;     // rythme max absolu
	det_inst[89][1] = 47;     //note reference
	det_inst[89][2] = 72;      // note max
	det_inst[89][3] = 24;      // note min

	//polysynthe 
	det_inst[90][0] = 600;     // rythme max absolu
	det_inst[90][1] = 47;     //note reference
	det_inst[90][2] = 72;      // note max
	det_inst[90][3] = 24;      // note min

	//choeur 
	det_inst[91][0] = 600;     // rythme max absolu
	det_inst[91][1] = 47;     //note reference
	det_inst[91][2] = 72;      // note max
	det_inst[91][3] = 24;      // note min

	//archet 
	det_inst[92][0] = 400;     // rythme max absolu
	det_inst[92][1] = 47;     //note reference
	det_inst[92][2] = 72;      // note max
	det_inst[92][3] = 24;      // note min

	//métallique
	det_inst[93][0] = 600;     // rythme max absolu
	det_inst[93][1] = 47;     //note reference
	det_inst[93][2] = 72;      // note max
	det_inst[93][3] = 24;      // note min

	//halo
	det_inst[94][0] = 600;     // rythme max absolu
	det_inst[94][1] = 47;     //note reference
	det_inst[94][2] = 72;      // note max
	det_inst[94][3] = 24;      // note min

	//balai
	det_inst[95][0] = 600;     // rythme max absolu
	det_inst[95][1] = 47;     //note reference
	det_inst[95][2] = 72;      // note max
	det_inst[95][3] = 24;      // note min

	//pluie de glace
	det_inst[96][0] = 600;     // rythme max absolu
	det_inst[96][1] = 47;     //note reference
	det_inst[96][2] = 72;      // note max
	det_inst[96][3] = 24;      // note min

	//trames sonores
	det_inst[97][0] = 600;     // rythme max absolu
	det_inst[97][1] = 47;     //note reference
	det_inst[97][2] = 72;      // note max
	det_inst[97][3] = 24;      // note min

	//cristal
	det_inst[98][0] = 600;     // rythme max absolu
	det_inst[98][1] = 47;     //note reference
	det_inst[98][2] = 72;      // note max
	det_inst[98][3] = 24;      // note min

	//atmosphere
	det_inst[99][0] = 600;     // rythme max absolu
	det_inst[99][1] = 47;     //note reference
	det_inst[99][2] = 72;      // note max
	det_inst[99][3] = 24;      // note min

	//brillance 
	det_inst[100][0] = 600;     // rythme max absolu
	det_inst[100][1] = 47;     //note reference
	det_inst[100][2] = 72;      // note max
	det_inst[100][3] = 24;      // note min

	//gobelins
	det_inst[101][0] = 600;     // rythme max absolu
	det_inst[101][1] = 47;     //note reference
	det_inst[101][2] = 72;      // note max
	det_inst[101][3] = 24;      // note min

	//echos 
	det_inst[102][0] = 600;     // rythme max absolu
	det_inst[102][1] = 47;     //note reference
	det_inst[102][2] = 72;      // note max
	det_inst[102][3] = 24;      // note min

	//espace sci-fi
	det_inst[103][0] = 600;     // rythme max absolu
	det_inst[103][1] = 47;     //note reference
	det_inst[103][2] = 72;      // note max
	det_inst[103][3] = 24;      // note min

	//sitar 
	det_inst[104][0] = 600;     // rythme max absolu
	det_inst[104][1] = 47;     //note reference
	det_inst[104][2] = 72;      // note max
	det_inst[104][3] = 24;      // note min

	//banjo
	det_inst[105][0] = 600;     // rythme max absolu
	det_inst[105][1] = 47;     //note reference
	det_inst[105][2] = 72;      // note max
	det_inst[105][3] = 24;      // note min

	//shamisen
	det_inst[106][0] = 600;     // rythme max absolu
	det_inst[106][1] = 47;     //note reference
	det_inst[106][2] = 72;      // note max
	det_inst[106][3] = 24;      // note min

	//koto
	det_inst[107][0] = 600;     // rythme max absolu
	det_inst[107][1] = 47;     //note reference
	det_inst[107][2] = 72;      // note max
	det_inst[107][3] = 24;      // note min

	//kalimba
	det_inst[108][0] = 600;     // rythme max absolu
	det_inst[108][1] = 47;     //note reference
	det_inst[108][2] = 72;      // note max
	det_inst[108][3] = 24;      // note min

	//cornemuse 
	det_inst[109][0] = 600;     // rythme max absolu
	det_inst[109][1] = 47;     //note reference
	det_inst[109][2] = 72;      // note max
	det_inst[109][3] = 24;      // note min

	//viole
	det_inst[110][0] = 500;     // rythme max absolu
	det_inst[110][1] = 47;     //note reference
	det_inst[110][2] = 72;      // note max
	det_inst[110][3] = 24;      // note min

	//shehnai
	det_inst[111][0] = 600;     // rythme max absolu
	det_inst[111][1] = 47;     //note reference
	det_inst[111][2] = 72;      // note max
	det_inst[111][3] = 24;      // note min

	//clochettes

	det_inst[112][0] = 600;     // rythme max absolu
	det_inst[112][1] = 47;     //note reference
	det_inst[112][2] = 72;      // note max
	det_inst[112][3] = 24;      // note min

	//agogo
	det_inst[113][0] = 600;     // rythme max absolu
	det_inst[113][1] = 47;     //note reference
	det_inst[113][2] = 72;      // note max
	det_inst[113][3] = 24;      // note min

	//batterie metallique
	det_inst[114][0] = 600;     // rythme max absolu
	det_inst[114][1] = 47;     //note reference
	det_inst[114][2] = 72;      // note max
	det_inst[114][3] = 24;      // note min

	//planchettes
	det_inst[115][0] = 600;     // rythme max absolu
	det_inst[115][1] = 47;     //note reference
	det_inst[115][2] = 72;      // note max
	det_inst[115][3] = 24;      // note min

	//timbales
	det_inst[116][0] = 600;     // rythme max absolu
	det_inst[116][1] = 24;     //note reference
	det_inst[116][2] = 40;      // note max
	det_inst[116][3] = 10;      // note min

	//tom mélodique
	det_inst[117][0] = 600;     // rythme max absolu
	det_inst[117][1] = 47;     //note reference
	det_inst[117][2] = 72;      // note max
	det_inst[117][3] = 24;      // note min

	//tambour synthetique
	det_inst[118][0] = 600;     // rythme max absolu
	det_inst[118][1] = 47;     //note reference
	det_inst[118][2] = 72;      // note max
	det_inst[118][3] = 24;      // note min

	//cymbale
	det_inst[119][0] = 600;     // rythme max absolu
	det_inst[119][1] = 47;     //note reference
	det_inst[119][2] = 72;      // note max
	det_inst[119][3] = 24;      // note min

	//guitare bruit de frette
	det_inst[120][0] = 600;     // rythme max absolu
	det_inst[120][1] = 47;     //note reference
	det_inst[120][2] = 72;      // note max
	det_inst[120][3] = 24;      // note min

	//respiration
	det_inst[121][0] = 600;     // rythme max absolu
	det_inst[121][1] = 47;     //note reference
	det_inst[121][2] = 72;      // note max
	det_inst[121][3] = 24;      // note min

	//rivage
	det_inst[122][0] = 300;     // rythme max absolu
	det_inst[122][1] = 47;     //note reference
	det_inst[122][2] = 72;      // note max
	det_inst[122][3] = 24;      // note min

	//gazouilli
	det_inst[123][0] = 600;     // rythme max absolu
	det_inst[123][1] = 47;     //note reference
	det_inst[123][2] = 72;      // note max
	det_inst[123][3] = 24;      // note min

	//sonnerie tel
	det_inst[124][0] = 600;     // rythme max absolu
	det_inst[124][1] = 47;     //note reference
	det_inst[124][2] = 72;      // note max
	det_inst[124][3] = 24;      // note min

	//hélicoptere
	det_inst[125][0] = 300;     // rythme max absolu
	det_inst[125][1] = 47;     //note reference
	det_inst[125][2] = 72;      // note max
	det_inst[125][3] = 24;      // note min

	//applaudissements
	det_inst[126][0] = 300;     // rythme max absolu
	det_inst[126][1] = 47;     //note reference
	det_inst[126][2] = 72;      // note max
	det_inst[126][3] = 24;      // note min

	//coup de feu
	det_inst[127][0] = 600;     // rythme max absolu
	det_inst[127][1] = 47;     //note reference
	det_inst[127][2] = 72;      // note max
	det_inst[127][3] = 24;      // note min

	// Liste de la voie basse des instruments
	
	//piano grand acoustique voie basse
	det_inst[128][0] = 600;     // rythme max absolu
	det_inst[128][1] = 20;     //note reference
	det_inst[128][2] = 36;      // note max
	det_inst[128][3] = 10;      // note min
	
	//piano acoustique
	det_inst[129][0] = 600;     // rythme max absolu
	det_inst[129][1] = 20;     //note reference
	det_inst[129][2] = 36;      // note max
	det_inst[129][3] = 10;      // note min

	//piano grand electrique
	det_inst[130][0] = 600;     // rythme max absolu
	det_inst[130][1] = 20;     //note reference
	det_inst[130][2] = 36;      // note max
	det_inst[130][3] = 10;      // note min

	//piano bastringue
	det_inst[131][0] = 600;     // rythme max absolu
	det_inst[131][1] = 20;     //note reference
	det_inst[131][2] = 36;      // note max
	det_inst[131][3] = 10;      // note min

	//piano electrique
	det_inst[132][0] = 600;     // rythme max absolu
	det_inst[132][1] = 20;     //note reference
	det_inst[132][2] = 36;      // note max
	det_inst[132][3] = 10;      // note min

	//piano effet chorus
	det_inst[133][0] = 400;     // rythme max absolu
	det_inst[133][1] = 20;     //note reference
	det_inst[133][2] = 36;      // note max
	det_inst[133][3] = 10;      // note min

	//clavecin
	det_inst[134][0] = 500;     // rythme max absolu
	det_inst[134][1] = 20;     //note reference
	det_inst[134][2] = 36;      // note max
	det_inst[134][3] = 10;      // note min

	//clavinet
	det_inst[135][0] = 500;     // rythme max absolu
	det_inst[135][1] = 20;     //note reference
	det_inst[135][2] = 36;      // note max
	det_inst[135][3] = 10;      // note min

	//celesta
	det_inst[136][0] = 800;     // rythme max absolu
	det_inst[136][1] = 20;     //note reference
	det_inst[136][2] = 36;      // note max
	det_inst[136][3] = 10;      // note min

	//Glockenspiel
	det_inst[140][0] = 800;     // rythme max absolu
	det_inst[140][1] = 20;     //note reference
	det_inst[140][2] = 36;      // note max
	det_inst[140][3] = 10;      // note min

	//boite a musique
	det_inst[141][0] = 800;     // rythme max absolu
	det_inst[141][1] = 20;     //note reference
	det_inst[141][2] = 36;      // note max
	det_inst[141][3] = 10;      // note min

	//Vibraphone
	det_inst[142][0] = 800;     // rythme max absolu
	det_inst[142][1] = 20;     //note reference
	det_inst[142][2] = 36;      // note max
	det_inst[142][3] = 10;      // note min

	//marimba
	det_inst[143][0] = 800;     // rythme max absolu
	det_inst[143][1] = 20;     //note reference
	det_inst[143][2] = 36;      // note max
	det_inst[143][3] = 10;      // note min

	//Xylophone
	det_inst[144][0] = 800;     // rythme max absolu
	det_inst[144][1] = 20;     //note reference
	det_inst[144][2] = 36;      // note max
	det_inst[144][3] = 10;      // note min

	//cloches tubulaires
	det_inst[145][0] = 800;     // rythme max absolu
	det_inst[145][1] = 20;     //note reference
	det_inst[145][2] = 36;      // note max
	det_inst[145][3] = 10;      // note min

	//Tympanon
	det_inst[146][0] = 800;     // rythme max absolu
	det_inst[146][1] = 20;     //note reference
	det_inst[146][2] = 36;      // note max
	det_inst[146][3] = 10;      // note min

	//Orgue hammond
	det_inst[147][0] = 500;     // rythme max absolu
	det_inst[147][1] = 20;     //note reference
	det_inst[147][2] = 36;      // note max
	det_inst[147][3] = 10;      // note min

	//Orgue a percussion
	det_inst[148][0] = 500;     // rythme max absolu
	det_inst[148][1] = 20;     //note reference
	det_inst[148][2] = 36;      // note max
	det_inst[148][3] = 10;      // note min

	//Orgue Rock
	det_inst[149][0] = 500;     // rythme max absolu
	det_inst[149][1] = 20;     //note reference
	det_inst[149][2] = 36;      // note max
	det_inst[149][3] = 10;      // note min

	//grandes orgues
	det_inst[150][0] = 300;     // rythme max absolu
	det_inst[150][1] = 20;     //note reference
	det_inst[150][2] = 36;      // note max
	det_inst[150][3] = 10;      // note min

	//harmonium
	det_inst[151][0] = 300;     // rythme max absolu
	det_inst[151][1] = 20;     //note reference
	det_inst[151][2] = 36;      // note max
	det_inst[151][3] = 10;      // note min

	//accordeon
	det_inst[152][0] = 600;     // rythme max absolu
	det_inst[152][1] = 20;     //note reference
	det_inst[152][2] = 36;      // note max
	det_inst[152][3] = 10;      // note min

	//Harmonica
	det_inst[153][0] = 400;     // rythme max absolu
	det_inst[153][1] = 20;     //note reference
	det_inst[153][2] = 36;      // note max
	det_inst[153][3] = 10;      // note min

	//accordeon tango
	det_inst[154][0] = 600;     // rythme max absolu
	det_inst[154][1] = 20;     //note reference
	det_inst[154][2] = 36;      // note max
	det_inst[154][3] = 10;      // note min

}
	
